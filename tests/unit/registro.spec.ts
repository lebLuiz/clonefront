import 'reflect-metadata';
import { AuthService } from "@/app/services/AuthService";
import {container} from 'tsyringe';
// jest.mock('@/app/services/AuthService');

describe('Registro', () => {
    it('Deve chamar authService.register uma unica vez', () => {
        const authService = container.resolve(AuthService);
        authService.register = jest.fn();
        authService.register({});

        expect(authService.register).toHaveBeenCalledTimes(1);
    });

    it('Deve retornar uma exception caso omite parâmetros obrigatórios ou informe parâmetros inválidos', () => {
        const parametros_obrigatorios = ['nome', 'email'];

        try {
            const authService = container.resolve(AuthService);
            authService.register({});
        } catch (error) {
            expect(error).toHaveProperty('errors')
            expect(error.message).toBe('Dados inválidos');

            parametros_obrigatorios.forEach(param => {
                error.errors.forEach((err: string) => {
                    if(new RegExp(param).test(err)) {
                        expect(err.includes(param)).toBeTruthy();
                    }
                })
            });
        }

    })
});