import 'reflect-metadata';
import { AuthService } from "@/app/services/AuthService";
import {container} from 'tsyringe';
jest.mock('@/app/services/AuthService');

describe('Login', () => {
    it('Deve validar o campo de email', () => {
        try {
            const authService = container.resolve(AuthService);
            authService.login('','senha');
        } catch (error) {
            expect(error.message).toBe('Dados inválidos');
            expect(error.errors.toString()).toMatch(/email/);
        }
    });

    it('Deve chamar login apenas uma vez.', async () => {
        const authService = container.resolve(AuthService);
        authService.login('email@email.com','senha');

        expect(authService.login).toHaveBeenCalledTimes(1);
    });

    it('Deve retornar o token do usuario ao efetuar o login', async () => {
        const authService = container.resolve(AuthService);
        const data = {'auth_token': '123'};
        authService.login = jest.fn().mockImplementation(() => data);
        const response = await authService.login('email@email.com','senha');
        
        expect(authService.login).toHaveBeenCalled();
        expect(response).toMatchObject(data);
    });
});