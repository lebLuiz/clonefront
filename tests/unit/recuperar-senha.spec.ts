import 'reflect-metadata';
import { AuthService } from "@/app/services/AuthService";
import {container} from 'tsyringe';

describe('Solicita recuperação de senha', () => {
    it('Deve verificar se o campo de email é válido', () => {
        try {
            const authService = container.resolve(AuthService);
            authService.recover('email_invalido');
        } catch (error) {
            expect(error.message).toBe('Dados inválidos');
            expect(error.errors.toString()).toMatch(/email/);
        }
    });

    it('Deve chamar authSservice.recover apenas uma vez', () => {
        const authService = container.resolve(AuthService);
        authService.recover = jest.fn();

        authService.recover('email@email.com');
        expect(authService.recover).toHaveBeenCalledTimes(1);
    });
});