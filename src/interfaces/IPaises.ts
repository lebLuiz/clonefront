export default interface IPaises {
  id:bigint;
  name:string;
  iso2:string;
  phonecode:string;
  currency:string;
  currency_symbol:string;
}