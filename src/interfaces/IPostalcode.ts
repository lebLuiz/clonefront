export interface IPostalcode {
  bairro:string;
  cidade:string;
  codigo_cidade:bigint;
  codigo_estado:bigint;
  codigo_pais:bigint;
  emoji:any;
  estado:string;
  logradouro:string;
  pais:string;
  phonecode:string;
}