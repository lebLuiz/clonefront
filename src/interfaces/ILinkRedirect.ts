export interface ILinkRedirect {
    label: string,
    srcOrpath: string
}