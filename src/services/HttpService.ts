import axios from 'axios';
import UserService from "./UserService";

const userService = new UserService();
const user = userService.getLoggedInUser();

const authToken = (user && user.hasOwnProperty('token')) ? user.token : '';

const instance = axios.create({
  baseURL: process.env.API_URL,
  headers: {
    'Content-Type': 'application/json',
    'Accept': 'application/json',
    'Authorization': 'Bearer ' + authToken
  }
});

/* 
instance.interceptors.request.use(config => {
  return config;
});

instance.interceptors.response.use(response => {
  NProgress.done();
  return response;
}); 
*/

export default instance
