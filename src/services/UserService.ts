export default class UserService {
    
    setLoggedInUser (userData: string) {
        return localStorage.setItem('user', userData);
    }
    
    getLoggedInUser () {
        const user: any = localStorage.getItem('user');
        
        return JSON.parse(user);
    }
    
    deleteLoggedInUser () {
        return localStorage.removeItem('user');
    }
}