import axios from 'axios';

export default class LoginService {
  axios: any;
  constructor() {
    this.axios = axios;
  }

  authenticate(email: string, password: string) {
    return this.axios.post('/api/authcheck', { email: email, password: password});
  }

}