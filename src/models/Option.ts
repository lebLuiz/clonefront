import { IOption } from "@/interfaces/IOption";

export default class Option implements IOption {
    value: any;
    label: string;
    disabled: boolean;
    
    constructor() {
        this.label = '';
        this.disabled = false;
    }
}