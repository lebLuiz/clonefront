import { ILinkRedirect } from "@/interfaces/ILinkRedirect";

export default class LinkRedirect implements ILinkRedirect {
    label: string;
    srcOrpath: string;

    constructor() {
        this.label = '';
        this.srcOrpath = '';
    }
}