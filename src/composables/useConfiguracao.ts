import {useStore} from 'vuex';
import {ref, computed} from 'vue';

export function useConfiguracao() {
    const store = useStore();
    const configuracaoSelecionada = ref({label: "", value:""});
    const selectTransportadorHabilitado = ref(false);
    const selectUsuarioReferenciaHabilidado = ref(false);
    const configuracoes = computed(() => {
        return store.getters['configuracoes/getTipoConfiguracoes'];
    });

    const handleConfiguracao = (configuracao: {label:string, value:string}) => {
        const {frete_documentos, frete_nao_documentos} = store.state['configuracoes'].configuracoes
        selectTransportadorHabilitado.value = false;
        selectUsuarioReferenciaHabilidado.value = false;
        
        if(configuracao.value == frete_documentos.value || configuracao.value == frete_nao_documentos.value) {
            selectTransportadorHabilitado.value = true;
            selectUsuarioReferenciaHabilidado.value = true;
        }

        configuracaoSelecionada.value = configuracao;
    }

    return {
        configuracoes,
        configuracaoSelecionada,
        selectTransportadorHabilitado,
        selectUsuarioReferenciaHabilidado,
        handleConfiguracao
    }
}