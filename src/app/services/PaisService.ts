import { autoInjectable } from "tsyringe";
import { PaisRepositoryInterface } from '../repositories/contracts/PaisRepositoryInterface';
import { PaisRepository } from '../repositories/PaisRepository';
import { AxiosPromise } from 'axios';

@autoInjectable()
export class PaisService {

    private _repository: PaisRepositoryInterface;

    public constructor(repository : PaisRepository) {
        this._repository = repository;
    }

    public getAll(): AxiosPromise<any>
    {
        return this._repository.index();
    }
}