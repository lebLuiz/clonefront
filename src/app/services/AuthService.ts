import { AuthRepositoryInterface } from "@/app/repositories/contracts/AuthRepositoryInterface";
import { AuthRepository } from "@/app/repositories/AuthRepository";
import {autoInjectable} from "tsyringe";
import { AxiosPromise, AxiosError } from "axios";
import {createValidator} from '../helpers/validator/validation';
import { IRegistro } from './types/IAuth';

@autoInjectable()
export class AuthService {

    private _repository: AuthRepositoryInterface;

    public constructor(repository : AuthRepository) {
        this._repository = repository;
    }

    public login(email: string, password: string): AxiosPromise<any>
    {
        createValidator({email, password}, {
            email:"required|email",
            password:"required"
        });

        return this._repository.login(email, password);
    }

    public recover(email: string)
    {
        createValidator({email}, {
            email:"required|email"
        });

        return this._repository.recover(email);
    }

    public register(data: Object): AxiosPromise<any>
    {
        const dataRegister = this.transformRegistro(data)
        createValidator(dataRegister, {
            codigo_tipo_conta:"required",
            codigo_pais:"required",
            codigo_cidade:"required",
            codigo_estado:"required",
            bairro: "required|string",
            logradouro:"required|string",
            cep: "required",
            numero:"required",
            tipo:"required|string",
            nome:"required|string",
            email:"required|email",
            senha:"required|string",
            telefone:"required|string",
            federal_tax_id:"required|string",
            descricao:"required"
        });

        return this._repository.register(dataRegister);
    }

    public async reset(id: bigint, data: Object)
    {
        try{
            const response = await this._repository.update(id, data);
            return response.data;
        }catch(error){
            console.log(error.response);
        }
    }

    private transformRegistro(data:any):IRegistro
    {
        let documento:string;
        if(data?.typeAccount?.value.toUpperCase() == 'PF')
            documento = data?.cpf
        else
            documento = data?.cnpj
        
        const telefone = `${data?.tel?.idd}${data?.tel?.phone}`;
        return {
            nome:data?.name,
            email: data?.email,
            senha: data?.password,
            cep:data?.cep,
            logradouro:data?.address,
            bairro:data?.district,
            numero:data?.number,
            complemento:data?.complement,
            codigo_cidade:data?.cityAddressSelected?.value,
            codigo_estado: data?.stateAddressSelected?.value,
            codigo_pais:data?.codigo_pais,
            codigo_tipo_conta:data?.codTypeAccount?.value,
            codigo_usuario:data?.associatedUserCode?.value,
            hash_usuario_associado:data?.hash_usuario_associado,
            federal_tax_id:documento,
            state_tax_id:data?.rg,
            telefone:telefone,
            descricao:data?.description,
            tipo: data?.typeAccount?.value,
        }
    }
}