export interface IRegistro {
  email: string;
  codigo_tipo_conta: number;
  senha: string;
  hash_usuario_associado: string;
  codigo_usuario: number;
  codigo_pais: number;
  codigo_estado: number;
  codigo_cidade: number;
  descricao: string;
  cep: string;
  logradouro: string;
  numero: string;
  bairro: string;
  complemento?: string;
  nome: string;
  telefone: string;
  tipo: string;
  federal_tax_id: string;
  state_tax_id: string;
}