import { EnderecoRepositoryInterface } from "@/app/repositories/contracts/EnderecoRepositoryInterface";
import { EnderecoRepository } from "@/app/repositories/EnderecoRepository";
import { autoInjectable } from "tsyringe";

@autoInjectable()
export class EnderecoService {

    private _repository: EnderecoRepositoryInterface;

    public constructor(repository : EnderecoRepository) {
        this._repository = repository;
    }

    public async get(id: bigint)
    {
        try{
            const response = await this._repository.show(id);
            return response.data;
        }catch(error){
            console.log(error.response);
        }
    }

    public async getAll()
    {
        try{
            const response = await this._repository.index();
            return response.data;
        }catch(error){
            console.log(error.response);
        }
    }

    public async create(data: Object)
    {
        try{
            const response = await this._repository.store(data);
            return response.data;
        }catch(error){
            console.log(error.response);
        }
    }

    public async update(id: bigint, data: Object)
    {
        try{
            const response = await this._repository.update(id, data);
            return response.data;
        }catch(error){
            console.log(error.response);
        }
    }

    public async delete(id: bigint)
    {
        try{
            const response = await this._repository.destroy(id);
            return response.data;
        }catch(error){
            console.log(error.response);
        }
    }

}