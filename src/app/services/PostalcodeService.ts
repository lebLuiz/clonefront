import { autoInjectable } from "tsyringe";
import { PaisRepositoryInterface } from '../repositories/contracts/PaisRepositoryInterface';
import { PaisRepository } from '../repositories/PaisRepository';
import { PostalcodeRepositoryInterface } from '../repositories/contracts/PostalcodeRepositoryInterface';
import { AxiosPromise } from 'axios';
import { PostalcodeRepository } from "../repositories/PostalcodeRepository";
import { createValidator } from "../helpers/validator/validation";

@autoInjectable()
export class PostalcodeService {

    private _repository: PostalcodeRepositoryInterface;

    public constructor(repository : PostalcodeRepository) {
        this._repository = repository;
    }

    public buscaDetalhesDoCep(country_code:string, cep:string): AxiosPromise<any>
    {
        createValidator({country_code, cep}, {
            cep: "required|digits:8",
            country_code:"required"
        });
        return this._repository.buscaDetalhesDoCep(country_code.toUpperCase(), cep);
    }
}