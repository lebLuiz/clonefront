import axios from "axios";
import config from "@/config/app";
import { injectable } from "tsyringe";
import {Store} from 'vuex';
import store from '@/config/store/store';

@injectable()
export class HttpClientService {

    protected _Config: typeof config;
    protected _store: Store<any>;

    public constructor() {
        this._Config = config;
        this._store = store;
    }

    public getHttpClient(){
        return axios.create();
    }

    public getBackURLClient()
    {
        const data: string|null = localStorage.getItem('auth_token');
        const auth_token: string = (data != null ? data : '');
        return this.getClient(process.env.VUE_APP_BASE_URL, auth_token);
    }

    public getBackClient()
    {
        const data: string|null = localStorage.getItem('auth_token');
        const auth_token: string = (data != null ? data : '');
        return this.getClient(process.env.VUE_APP_BASE_URL_BACK, auth_token);
    }

    public getApiCotacaoClient()
    {
        const data: string|null = this._store.getters['usuario/usuario'];
        const Usuario: Object = (data != null ? JSON.parse(data) : {});
        // @ts-ignore
        return this.getClient(process.env.VUE_APP_BASE_URL_BACK, (Usuario.hasOwnProperty('api_token') ? Usuario.api_token : ''));
    }

    public getApiRemessaClient()
    {
        const data: string|null = this._store.getters['usuario/usuario'];
        const Usuario: Object = (data != null ? JSON.parse(data) : {});
        // @ts-ignore
        return this.getClient(process.env.VUE_APP_BASE_URL_BACK, (Usuario.hasOwnProperty('api_token') ? Usuario.api_token : ''));
    }

    public getApiCaixaClient()
    {
        const data: string|null = this._store.getters['usuario/usuario'];
        const Usuario: Object = (data != null ? JSON.parse(data) : {});
        // @ts-ignore
        return this.getClient(process.env.VUE_APP_BASE_URL_BACK, (Usuario.hasOwnProperty('api_token') ? Usuario.api_token : ''));
    }

    public getApiItemClient()
    {
        const data: string|null = this._store.getters['usuario/usuario'];
        const Usuario: Object = (data != null ? JSON.parse(data) : {});
        // @ts-ignore
        return this.getClient(process.env.VUE_APP_BASE_URL_BACK, (Usuario.hasOwnProperty('api_token') ? Usuario.api_token : ''));
    }

    private getClient(base_url: string|undefined, token: string)
    {
        const config : Object = {
            baseURL: base_url,
            withCredentials:true,
            headers : {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + token
            }
        };
        return axios.create(config);
    }
    
}