import { autoInjectable } from "tsyringe";
import { HomeRepositoryInterface } from '../repositories/contracts/HomeRepositoryInterface';
import {HomeRepository} from '../repositories/HomeRepository';
import { AxiosPromise } from 'axios';

@autoInjectable()
export class HomeService {

    private _repository: HomeRepositoryInterface;

    public constructor(repository : HomeRepository) {
        this._repository = repository;
    }

    public getSpec(): AxiosPromise<any>
    {
        return this._repository.index();
    }
}