import { BaseRepository } from "@/app/repositories/BaseRepository";
import { AxiosPromise } from "axios";
import { Pais } from '../models/Pais';
import { PaisRepositoryInterface } from './contracts/PaisRepositoryInterface';

export class PaisRepository extends BaseRepository implements PaisRepositoryInterface
{
    protected _model: Pais = new Pais();

    index(): AxiosPromise<any> {
        return this._http_client.getBackClient().get('/' + this._model.name);
    }
}