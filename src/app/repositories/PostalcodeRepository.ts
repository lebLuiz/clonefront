import { BaseRepository } from "@/app/repositories/BaseRepository";
import { AxiosPromise } from "axios";
import { PostalcodeRepositoryInterface } from './contracts/PostalcodeRepositoryInterface';
import { Postalcode } from '../models/Postalcode';

export class PostalcodeRepository extends BaseRepository implements PostalcodeRepositoryInterface
{
    protected _model: Postalcode = new Postalcode();

    index(): AxiosPromise<any> {
        return this._http_client.getBackClient().get('/' + this._model.name);
    }

    buscaDetalhesDoCep(country_code:string, cep:string): AxiosPromise<any> {
        return this._http_client.getBackClient().post(`/${this._model.name}/${country_code}/${cep}`);
    }
}