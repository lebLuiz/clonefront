import { BaseRepositoryInterface } from "@/app/repositories/contracts/BaseRepositoryInterface";

export interface AuthRepositoryInterface extends BaseRepositoryInterface {

    register(data: Object) : Promise<any>;

    login(email: string, password: string) : Promise<any>;

    recover(email: string) : Promise<any>;

    reset(email: string) : Promise<any>;

}