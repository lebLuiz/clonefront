import { BaseRepositoryInterface } from "@/app/repositories/contracts/BaseRepositoryInterface";
import { AxiosPromise } from 'axios';

export interface PostalcodeRepositoryInterface extends BaseRepositoryInterface {
  buscaDetalhesDoCep(country_code: string, cep:string) : AxiosPromise<any>;
}