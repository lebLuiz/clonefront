export interface BaseRepositoryInterface {

    index() : Promise<any>;

    show(id: bigint) : Promise<any>;

    store(data: Object) : Promise<any>;

    update(id: bigint, data: Object) : Promise<any>;

    destroy(id: bigint) : Promise<any>;

}