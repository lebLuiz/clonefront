import { BaseRepository } from "@/app/repositories/BaseRepository";
import { EnderecoRepositoryInterface } from "@/app/repositories/contracts/EnderecoRepositoryInterface";
import { Endereco } from "@/app/models/Endereco";

export class EnderecoRepository extends BaseRepository implements EnderecoRepositoryInterface
{

    protected _model: Endereco = new Endereco();

}