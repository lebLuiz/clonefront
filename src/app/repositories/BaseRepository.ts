import { BaseRepositoryInterface } from '@/app/repositories/contracts/BaseRepositoryInterface';
import { BaseModel } from "@/app/models/BaseModel";
import { AxiosPromise } from "axios";
import { HttpClientService } from "@/app/services/HttpClientService";
import { autoInjectable, inject } from "tsyringe";

// @ts-ignore
@autoInjectable()
export abstract class BaseRepository implements BaseRepositoryInterface {

    protected abstract _model: BaseModel;

    constructor(protected _http_client: HttpClientService) {}

    index(): AxiosPromise<any> {
        return this._http_client.getBackClient().get('/' + this._model.name + '/index');
    }

    show(id: bigint): AxiosPromise<any> {
        return this._http_client.getBackClient().get('/' + this._model.name + '/' + id);
    }

    store(data: Object): AxiosPromise<any> {
        return this._http_client.getBackClient().post('/' + this._model.name + '/index', data);
    }

    update(id: bigint, data: Object): AxiosPromise<any> {
        return this._http_client.getBackClient().put('/' + this._model.name + '/' + id, data);
    }

    destroy(id: bigint): AxiosPromise<any> {
        return this._http_client.getBackClient().delete('/' + this._model.name + '/' + id);
    }

}