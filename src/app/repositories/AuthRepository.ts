import { BaseRepository } from "@/app/repositories/BaseRepository";
import { Auth } from "@/app/models/Auth";
import { AuthRepositoryInterface } from "@/app/repositories/contracts/AuthRepositoryInterface";
import { AxiosPromise } from "axios";

export class AuthRepository extends BaseRepository implements AuthRepositoryInterface
{

    protected _model: Auth = new Auth();

    async login(email: string, password: string): Promise<AxiosPromise<any>> {
        const data : Object = {
            'email' : email,
            'senha' : password
        };

        await this._http_client.getBackURLClient().get('/sanctum/csrf-cookie');
        return this._http_client.getBackClient().post('/' + this._model.name + '/login', data);

    }

    recover(email: string): AxiosPromise<any> {
        return this._http_client.getBackClient().post('/' + this._model.name + '/forgot-password', { email: email });
    }

    register(data: Object): AxiosPromise<any> {
        return this._http_client.getBackClient().post('/' + this._model.name + '/register', data);
    }

    reset(email: string): AxiosPromise<any> {
        return this._http_client.getBackClient().post('/' + this._model.name + '/reset-password', { email: email });
    }

}