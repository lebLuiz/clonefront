import { AxiosPromise } from "axios";
import { Home } from '../models/Home';
import { HomeRepositoryInterface } from './contracts/HomeRepositoryInterface';
import { BaseRepository } from "@/app/repositories/BaseRepository";

export class HomeRepository extends BaseRepository implements HomeRepositoryInterface
{
    protected _model: Home = new Home();

    index(): AxiosPromise<any> {
        return this._http_client.getBackClient().get('');
    }
}