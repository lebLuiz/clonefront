import {validators} from './validators'

class WarningException
{
    private errors: string[];
    private message: string;

    public constructor(errors : string[]) {
        this.message = "Dados inválidos";
        this.errors = errors;
    }
}

const createValidator = (data: object, dataRules:object) => {
    const errors:string[] = [];
    Object.entries(dataRules).forEach((dataRule: [string, string]) => {
        const [key, stringRules] = dataRule;
        const rules = stringRules.split('|');

        rules.forEach(rule => {
            const complement = rule.split(':');
            let hasValidate:any = ""; 
            if(complement.length === 2) {
                const [complementRule, complementSpec] = complement;
                if(validators.hasOwnProperty(complementRule)) {
                    // @ts-ignore
                    hasValidate = validators[complementRule](data, key, complementSpec);
                }
            } else {
                if(validators.hasOwnProperty(rule)) {
                    // @ts-ignore
                    hasValidate = validators[rule](data, key);
                }
            }

            if(hasValidate) errors.push(hasValidate)
        })
    });

    if(errors.length) {
        throw new WarningException(errors);
    }
}

export {
    createValidator
};