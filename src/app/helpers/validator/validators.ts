function isValidEmail(email:string) {
    return /\@/.test(email);
}

const validators = {
    string(data:any, key:string):string|undefined {
        if(data[key] && (typeof data[key] !== "string"))
            return `O campo ${key} precisa ser uma string`;
    },  
    required(data:any, key:string):string|undefined {
        if(!data.hasOwnProperty(key) || !data[key])
            return `O campo ${key} é obrigatório.`;
    },

    email(data:any, key:string):string|undefined {
        if(data[key] && !isValidEmail(data[key]))
            return `O campo ${key} precisa ser um email válido.`
    },

    digits(data:any, key:string, digits:number): string|undefined {
        if(data[key] && (data[key].length < digits || data[key].length > digits))
            return `O campo ${key} precisa ter ${digits} digitos`
    }
}

export {
    validators
}