import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'

import Inicio from '@/pages/inicio/Inicio.vue';
import Duvidas from '@/pages/duvida/Duvidas.vue';
import Entrar from '@/pages/entrar/Entrar.vue';
import ForgotPassword from '@/pages/forgot_password/ForgotPassword.vue';
import FreteUpload from '@/pages/frete_upload/FreteUpload.vue';
import Register from '@/pages/register/Register.vue';
import Contato from '@/pages/contato/Contato.vue';
import Configuracoes from '@/pages/configuracoes/Configuracoes.vue';
import Canais from '@/pages/canais/Canais.vue';
import AdicionarCanal from '@/pages/canais/AdicionarCanal.vue';
import EditarCanal from '@/pages/canais/EditarCanal.vue';
import Permissoes from '@/pages/permissoes/Permissoes.vue';
import EditarPermissao from '@/pages/permissoes/EditarPermissao.vue';

interface ShipRoute {
    visible?:boolean;
    label?:string;
    icon?:any;
}

const routes: Array<RouteRecordRaw & ShipRoute> = [
    {
        path: '/',
        redirect: '/login',
        visible: false,
    },

    {
        name: 'abstract',
        path: '/abstract',
        component: () => import(/* webpackChunkName: "Home" */ '@/pages/home/Resumo.vue'),
        label: 'Resumo',
        visible: true,
        icon: require('@/assets/icons/icone_resumo.svg')
    },

    {
        name: 'services',
        path: '/services',
        component: () => import(/* webpackChunkName: "Home" */ '@/pages/home/servico/Servico.vue'),
        label: 'Serviços',
        visible: true,
        icon: require('@/assets/icons/icone_servicos.svg')
    },

    {
        name: 'pedidos',
        path: '/pedidos',
        component: Inicio,
        label: 'Pedidos',
        visible: true,
        icon: require('@/assets/icons/icone_pedidos.svg')
    },

    {
        name: 'tabela_frete',
        path: '/tabela_frete',
        component: Inicio,
        label: 'Tabela de Frete',
        visible: true,
        icon: require('@/assets/icons/icone_pin.svg')
    },

    {
        name: 'usuarios',
        path: '/usuarios',
        component: Inicio,
        label: 'Usuários',
        visible: true,
        icon: require('@/assets/icons/icone_usuarios.svg')
    },

    {
        name: 'logs',
        path: '/logs',
        component: Inicio,
        label: 'Logs',
        visible: true,
        icon: require('@/assets/icons/icone_logs.svg')
    },

    {
        name: 'regras_cotacao',
        path: '/regras_cotacao',
        component: Inicio,
        label: 'Regras de Cotação',
        visible: true,
        icon: require('@/assets/icons/icone_editar.svg')
    },

    {
        name: 'canais',
        path: '/canais',
        component: Canais,
        label: 'Canais',
        visible:true,
        icon: require('@/assets/icons/icone_logs.svg')
    },

    {
        name: 'adicionar_canal',
        path:'/canais/adicionar',
        component:AdicionarCanal
    },

    {
        name: 'editar_canal',
        path:'/canais/:canal_id/editar',
        component:EditarCanal,
        props:true
    },

    {
        name: 'permissoes',
        path:'/permissoes',
        component:Permissoes,
        label: 'Permissões',
        visible:true,
        icon: require('@/assets/icons/icone_lock.svg')
    },

    {
        name: 'editar_permissao',
        path:'/permissoes/:codigo_permissao/editar',
        component:EditarPermissao,
        props:true
    },

    {
        name: 'configuracoes',
        path: '/configuracoes',
        component: Configuracoes,
        label: 'Configurações',
        visible: true,
        icon: require('@/assets/icons/configs.svg')
    },

    {
        name: 'profile',
        path: '/profile',
        component: () => import(/* webpackChunkName: "Home" */ '@/pages/home/Profile.vue'),
    },

    /* {
        name: 'sair',
        path: '/sair',
        component: Inicio,
        label: 'Sair',
        visible: true,
        icon: require('@/assets/icons/icone_sair.svg')
    }, */


    //antigos
    {
        name: 'init',
        path: '/init',
        component: Inicio,
        label: 'Init',
        visible: false,
        icon: require('@/assets/icons/icone_servicos.svg')
    },

    {
        name: 'doubts',
        path: '/doubts',
        component: Duvidas,
        visible: false,
    },

    {
        name: 'login',
        path: '/login',
        component: Entrar,
        visible: false,
    },

    {
        name: 'forgotPassword',
        path: '/forgot_password',
        component: ForgotPassword,
        visible: false,
    },

    {
        name: 'register',
        path: '/register',
        component: Register,
        visible: false,
    },

    {
        name: 'contact',
        path: '/contact',
        component: Contato,
        visible: false,
    },

    {
        name: 'fileUpload',
        path: '/file_upload',
        component: FreteUpload,
        visible: false,
    },
];

const router = createRouter({
    history: createWebHistory(),
    routes
});
  
// router.beforeEach((to, from, next) => {
//     if (
//         !localStorage.user 
//         && to.name != 'login' 
//         && to.name != 'register'
//     ) next('/login'); 
//     else next();
// });

export default router;