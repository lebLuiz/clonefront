export default {
    termsOfUse: 'Terms of Use',

    message: {
        hello: 'Hello World!',
        enterLogin: 'Enter',
        amClientLogin: `I'm already a customer`,

        enterMsg: 'Login',
        signUpMsg: 'Sign Up',
        forgotPasswordMsg: 'Forgot your password?',
        recoverPassword: 'Recover Password',

        makeRegistration: 'Make your registration',
        completeRegistration: 'Complete your registration',

        freightUpload: 'FREIGHT CHART - LOAD',

        finishRegister: 'Registration completed successfully',
    },

    messageActions: {
        send: 'Send',

        txtBtnBack: 'Return',
        txtBtnAdvance: 'Advance',
        txtBtnFinish: 'Finish',
        txtBtnUpdate: 'Atualizar',

        txtBtnBackToLogin: 'Back to login',
    },

    titleLabels: {
        email: 'E-mail',
        password: 'Password',
        confirmPassword: 'Confirm password',

        // Regitration:
        fullName: 'Full name',
        idd: 'IDD',
        phone: 'Telephone',
        branchPhone: 'Branch',
        address: 'Address',
        numberAddress: 'Number',
        districtAddress: 'District',
        complementAddress: 'Complement',
        stateAddress: 'State',
        cityAddress: 'City',
        countryAddress: 'Country',

        accountType: 'Account type',
        accountCode: 'Account code',
        associatedUserCode: 'Associated user code',

        numberOfCPF: 'Number of CPF',
        numberOfCNPJ: 'Number of CNPJ',
        numberOfRG: 'Number of RG',

        typeGeneric: 'Type',
        codUser: 'Code of user',
        transporter: 'Transporter',
        
        selectFileTableDoc: 'Select the file from the Documents Table',
        selectFileTableNotDoc: 'Select file from Table No Documents',
        selectClientOrShipSmart: 'Choose the customer or Shipsmart',
        selectTypeAccount: 'Select account type',

        csvFileOnly: 'CSV file only',
        chooseFile: 'Choose file',
    },

    messageNotification: {
        fieldIsEmpty: 'Field is empty',
        defaultMsgErrorEmail: 'Please enter a valid email',
        defaultSixMinAndSpecialCharacters: 'Enter at least six characters, with at least one number or special character.*',
        defaultSixMinAndSpecialCharactersWithParentheses: 'Enter at least six characters (uppercase and lowercase distinction) with at least one number or special character.',
        receivePassword: 'As soon as you click on recover, you will receive your new password in your registration email. If the email has not arrived, check your spam/junk box.',
        
        justNumber: 'Fill in the field with numbers',
        fillTheField: 'Fill the field',
        fillFields: 'Fill in the fields to advance',
        notAllowedEmptyField: 'No empty field allowed',
        insertValidIdd: 'Please enter a valid IDD',
        insertValidPhone: 'Please enter a valid phone number',
        passwordsNotMatch: 'Passwords do not match',

        correctCep: 'Correctly fill in the CEP',
        selectCodAccount: 'Select an account code',
        selectAssociatedCod: 'Select an associated code',

        selectType: 'Select a type',
        selectCodUser: 'Select a code user',
        selectTransporter: 'Select a transporter',
        selectFileCsv: `Select a file ".csv"`,

        rgInvalid: 'RG is invalid',
        cpfInvalid: 'CPF is invalid',
        cnpjInvalid: 'CNPJ is invalid',

        informTheCountry: 'Inform the country',
        informTheCity: 'Inform the city',
        informTheState: 'Inform the state',

        backToLoginAccessAccount: 'Go back to the login screen to access your account',
    },

    messageLinksRedirect: {
        init: 'Start',
        doubts: 'Doubts',
        login: 'Enter',
        contact: 'Contact',
    },
}