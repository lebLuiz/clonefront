export default {
    termsOfUse: 'Termos de Uso',

    message: {
        hello: 'Olá mundo!',
        enterLogin: 'Entrar',
        amClientLogin: `Já sou cliente`,
        
        enterMsg: 'Entrar',
        signUpMsg: 'Cadastrar',
        forgotPasswordMsg: 'Esqueceu sua senha?',
        recoverPassword: 'Recuperar senha',

        makeRegistration: 'Faça seu cadastro',
        completeRegistration: 'Finalize seu cadastro',

        freightUpload: 'TABELA DE FRETE - UPLOAD',

        finishRegister: 'Cadastro finalizado com sucesso',
    },

    messageActions: {
        send: 'Enviar',

        txtBtnBack: 'Voltar',
        txtBtnAdvance: 'Avançar',
        txtBtnFinish: 'Finalizar',
        txtBtnUpdate: 'Atualizar',

        txtBtnBackToLogin: 'Voltar para login',
    },

    titleLabels: {
        email: 'E-mail',
        password: 'Senha',
        confirmPassword: 'Confirmar senha',

        // Regitration:
        fullName: 'Nome completo',
        idd: 'DDI',
        phone: 'Telefone',
        branchPhone: 'Ramal',
        address: 'Endereço',
        numberAddress: 'Número',
        districtAddress: 'Bairro',
        complementAddress: 'Complemento',
        stateAddress: 'Estado',
        cityAddress: 'Cidade',
        countryAddress: 'País',

        accountType: 'Tipo da conta',
        accountCode: 'Código da conta',
        associatedUserCode: 'Código usuário associado',

        numberOfCPF: 'Número do CPF',
        numberOfCNPJ: 'Número do CNPJ',
        numberOfRG: 'Número do RG',

        typeGeneric: 'Tipo',
        codUser: 'Código do usuario',
        transporter: 'Transportador',
        
        selectFileTableDoc: 'Selecione o arquivo da Tabela Documentos',
        selectFileTableNotDoc: 'Selecione o arquivo da Tabela Não Documentos',
        selectClientOrShipSmart: 'Escolha o cliente ou a Shipsmart',
        selectTypeAccount: 'Selecione o tipo da conta',
        
        csvFileOnly: 'Apenas arquivo CSV',
        chooseFile: 'Escolher arquivo',

        description: 'Descrição'
    },

    messageNotification: {
        fieldIsEmpty: 'Campo está vázio',
        defaultMsgErrorEmail: 'Por favor, digite um email válido',
        defaultSixMinAndSpecialCharacters: 'Digite no mínimo seis caracteres, com pelo menos um número ou caractere especial.*',
        defaultSixMinAndSpecialCharactersWithParentheses: 'Digite no mínimo seis caracteres(distinção entre maiúsculo e minúsculo) com pelo menos um número ou caractere especial.',
        receivePassword: 'Assim que clicar em recuperar, você receberá sua nova senha no seu e-mail de cadastro.Caso o e-mail não tenha chegado cheque sua caixa de spam/lixo eletrônico.',
        
        justNumber: 'Preencha o campo com números',
        fillTheField: 'Preencha o campo',
        fillFields: 'Preencha os campos para avançar',
        notAllowedEmptyField: 'Não é permitido campo vázio',
        insertValidIdd: 'Insira um DDI válido',
        insertValidPhone: 'Insira um número de telefone válido',
        passwordsNotMatch: 'Senhas não conferem',

        correctCep: 'Preencha corretamente o CEP',
        selectCodAccount: 'Selecione um código da conta',
        selectAssociatedCod: 'Selecione um código associado',

        selectType: 'Selecione um tipo',
        selectCodUser: 'Selecione um código de usuário',
        selectTransporter: 'Selecione um transportador',
        selectFileCsv: `Selecione um arquivo ".csv"`,

        rgInvalid: 'RG está inválido',
        cpfInvalid: 'CPF está inválido',
        cnpjInvalid: 'CNPJ está inválido',

        informTheCountry: 'Informe o país',
        informTheCity: 'Informe a cidade',
        informTheState: 'Informe o estado',

        backToLoginAccessAccount: 'Volte para a tela de login para acessar sua conta',
    },

    messageLinksRedirect: {
        init: 'Início',
        doubts: 'Dúvidas',
        login: 'Entrar',
        contact: 'Contato',
    },
}