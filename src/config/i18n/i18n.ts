import { createI18n } from 'vue-i18n'
import en from './messages/en'
import pt_BR from './messages/pt_BR'

const messages = {
    en: en,
    pt_BR: pt_BR,
}

export default createI18n({
    useScope: 'global',
    locale: 'pt_BR', // set locale
    messages, // set locale messages
});