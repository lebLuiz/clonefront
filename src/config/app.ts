export default {
    env: 'local',//local, staging, production
    base_url: 'http://shipapp.localhost',
    base_url_back: 'http://shipapp.localhost/api',
    base_url_api_cotacao: 'http://shipapi.localhost/cotation',
    base_url_api_remessa: 'http://shipapi.localhost/shipping',
    base_url_api_caixa: 'http://shipapi.localhost/box',
    base_url_api_item: 'http://shipapi.localhost/item',
}