export default {
    namespaced:true,
    state: {
        configuracoes: {
            impostos: {
                label: "Impostos",
                value: 1,
            },
            duty: {
                label: "Duty",
                value: 2
            },
            frete_documentos: {
                label: "Frete Documentos",
                value:3
            },
            frete_nao_documentos: {
                label: "Frete Não Documentos",
                value: 4
            },
            isencao_de_origem: {
                label: "Isenção de Origem",
                value:5
            },
            taxas_adicionais: {
                label: "Taxas Adicionais",
                value: 6
            }
        }
    },
    mutations: {
        SET_CONFIGURACOES(state:any, payload:any) {
            state.configuracoes = payload;
        }
    },
    getters: {
        getTipoConfiguracoes(state:any) {
            return Object.values(state.configuracoes);
        }
    }
}