export default {

  namespaced: true,
  state: {
    usuario: {
      nome:"",
      email:""
    },
  },

  getters: {
    usuario(state:any): object {
      return state.usuario;
    },
    token(): string|null {
      return localStorage.getItem('auth_token');
    }
  },

  mutations: {
    SET_USUARIO(state: any, value: object) {
      state.usuario = value;
    },
  },

  action: {
    setUsuario({ commit }: any , value: any) {
      commit('SET_USUARIO', value);
    },
  }
}
