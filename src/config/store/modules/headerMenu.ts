export default {
    namespaced: true,
    state: {
        activeHamburguerOptions: false,
    },
    getters: {

    },
    mutations: {
        setActiveHamburguerOptions(state: any, value: any) {
            state.activeHamburguerOptions = value;
        }
    },
    action: {

    }
}
