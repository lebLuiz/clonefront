export default {
    namespaced: true,
    state: {
        Alerta: null,
    },
    getters: {
        alerta(state: any) {
            return state.Alerta;
        }
    },
    mutations: {
        SET_ALERTA(state: any, value: any) {
            state.Alerta = value;
        }
    },
    action: {

    }
}
