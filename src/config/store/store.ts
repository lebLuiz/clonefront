import { createStore } from 'vuex'

import global from './modules/global'
import headerMenu from './modules/headerMenu'
import cotacao from './modules/cotacao'
import remessa from './modules/remessa'
import usuario from './modules/usuario'
import configuracoes from './modules/configuracoes'
import { container } from 'tsyringe'
// import { HomeService } from '@/app/services/HomeService';

export default createStore({
    state: {
        spec: {
            tipo_contas: {}
        }
    },
    mutations: {
        UPDATE_SPEC(state, payload) {
            state.spec = payload;
        }
    },
    actions: {
        // async carregaSpec({commit}) {
        //     const homeService = container.resolve(HomeService);
        //     const data = await homeService.getSpec();
        //     commit('UPDATE_SPEC', data.data);
        // }
    },
    getters: {
        getTipoContas: state => {
            return Object.values(state.spec.tipo_contas);
        }
    },
    modules: {
        global,
        headerMenu,
        cotacao,
        remessa,
        usuario,
        configuracoes
    }
});
