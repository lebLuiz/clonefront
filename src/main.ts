/* eslint-disable */
import 'reflect-metadata';
import { createApp } from 'vue';
import App from './App.vue';
import { container } from "tsyringe";
import '@/assets/css/tailwind.css';
// import '@/sass/styles.scss';

import store from '@/config/store/store';
import router from '@/config/router/router';
import i18n from '@/config/i18n/i18n';
import Maska from 'maska';

const app = createApp(App);

app.use(i18n);
app.use(store);
app.use(router);

/**
 * Dependency Injection Example
 * */
//let AuthService: AuthService = container.resolve(AuthService);

app.directive('maska', Maska);

app.mount('#app');
